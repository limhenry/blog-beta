---
layout: post
title:  "Xperia M2 Review"
date:   2015-02-03 12:00:00
---

Sony Xperia M2, the phone I am using right now.

Sony introduced this mid-range phone, Xperia M2 in CES 2014 and released in Q2 ‘14.

The price range is around USD300 (it’s around MYR 799 due today).

It’s not cheap, and I strongly NOT RECOMMENDING you if you wanna to buy a new Android phone.

Why? It’s that not good? No, it’s nice, but please don’t buy an old phone.

Anyway, let’s talk about this phone. Design, it’s nice. It has the (nearly) same design with Sony flagship devices like Z3, Z2 and Z1. But it is not using glass but plastic. That plastic is like a fingerprint-magnet, just holding that phone for a few seconds without cover, the back of phone will be full of fingerprint. So, a phone cover is highly recommended if you are using Xperia M2 right now.

Performance, it’s like a normal mid-range Android smartphone, it’s not so slow, normal browsing, Facebook, all no problem. Games? It still can run “Asphalt 8: Airborne” without any lag (the loading speed might be a bit bit longer ( ͡° ͜ʖ ͡°) ).

Camera. Firmware problem and it’s solved. Xperia M2 released to public with Android 4.3, Jelly Bean. During that time, the quality of camera is a bit horrible, especially the image noise. Luckily the problem solved after the Kitkat update. By the way, it’s running with a 8MP camera, with Full HD support. 

Front camera of Xperia M2? I just ignore that camera. The front camera is ‘unusable’. A VGA-front camera, that’s is totally horrible. If you are a selfie-lover, just simply don’t buy this phone. Brought already, use the rear camera. Thanks to the Camera button, you still can take selfie with that rear-camera, just need a little practise and skill.

Speaker. It’s quite loud, thanks to Sony’s xLOUD technology and ClearPhase. You can enjoy a ‘not bad’ quality with Xperia M2. 

BUT….  my phone having the biggest problem in this world (it look like I am the only one having this problem), which is the sound output (speaker and earphone, but no problem with Bluetooth earphone/speaker). The sound will stop playing randomly for 5 seconds -1 minute when the screen is off. I have no way to solve it, even after asking in XDA and Sony Support.

Conclusion: DON’T BUY.it’s not so cheap anyway. Buy Nexus 5 instead. Nexus 5 is a great phone but costs you around MYR 1200. Want cheaper, buy Moto G (second gen if possible)

Note: there is still no news about Android 5.0, Lollipop official update for Xperia M2. Sad news T.T

<div class="videoWrapper">
    <!-- Copy & Pasted from YouTube -->
    <iframe width="560" height="349" src="http://www.youtube.com/embed/Hgm7sPbXeVk" frameborder="0" allowfullscreen></iframe>
</div>



