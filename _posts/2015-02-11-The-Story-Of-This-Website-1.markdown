---
layout: post
title:  "The Story of This Website #1"
date:   2015-02-11 15:30:00
---

It's time to write something again.

Today, I have nothing special and important to write. But, I am excited to announce that the domain (URL) of this website is moved to [limhenry.me](http://limhenry.me). Shorter URL, so it's easier to remember!

Anyway, you still can visit limhenry.github.io but it will redirect automatically to limhenry.me

Just 'upgrade' the domain, all posts still remain same, don't worry.

By the way, I am going to write a very special review (story, something like that) and post it maybe tomorrow (evening, hopefully).

Stay tuned and remember to subscribe to my [newsletter](http://eepurl.com/bdrwRz)

Again, feel free to visit [limhenry.me](limhenry.me). And wish you Happy Chinese New Year, Huat Ah! (is that too early?)

*PS: sometime, can you disable your AdBlock when you visit this blog? Haha :)

